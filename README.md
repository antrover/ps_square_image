# README #

### What is this repository for? ###

* This photoshop action will resize an image and fit it into a 1080px X 1080px square. This is useful for posting images to Instagram in a square format.
* Version 1.0

### How do I get set up? ###

1. Clone this repo
2. Open Photoshop
3. Open an image
4. In Photoshop, click on 'File', 'Scripts', 'Browse'
5. Browse to the repo you just cloned and select 'squared_photo.jsx'


### Using this script in an action ###

1. Clone this repo
2. Open Photoshop
3. Open an image
4. In the 'Actions' menu, create a new Action (click on the '+' icon at the bottom of the panel). Name the action whatever you want. (example: 'Square Photo')
5. Press 'Begin Recording' (the circle icon at the bottom of the panel)
6. Click on 'File', 'Scripts', 'Browse'
7. Browse to the repo you just cloned and select 'squared_photo.jsx'
8. Click on 'Stop playing / recording' (bottom of the Action panel)
9. You should now see a new action called 'Square Photo' in the Action panel. Use this Action whenever you want to square a new photo.