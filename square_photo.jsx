var TARGET_WIDTH_HEIGHT = parseFloat(1020);
var CANVAS_WIDTH_HEIGHT = 1080;

// Set pixels as prefered units of measure
app.preferences.rulerUnits = Units.PIXELS;

var doc = app.activeDocument;

// Set background color to white
var color = app.backgroundColor;
color.rgb.red = 255;
color.rgb.green = 255;
color.rgb.blue = 255;
app.backgroundColor = color;

// Width and height of the doc
var current_height = parseFloat(doc.height);
var current_width = parseFloat(doc.width);

var new_image_width;
var new_image_height;
var ratio;

if(current_height > current_width){
    // vertical
    ratio = TARGET_WIDTH_HEIGHT / current_height;
    new_image_width = ratio * current_width;   
    new_image_height = TARGET_WIDTH_HEIGHT;
}else if(current_width > current_height){
    // horizontal
    ratio = TARGET_WIDTH_HEIGHT / current_width;
    new_image_height = ratio * current_height;
    new_image_width = TARGET_WIDTH_HEIGHT;
 }else {
    // square
    new_image_height = TARGET_WIDTH_HEIGHT;
    new_image_width = TARGET_WIDTH_HEIGHT;
}

// Resize the image and resize the canvas
doc.resizeImage(UnitValue(new_image_width,"px"), UnitValue(new_image_height,"px"), null, ResampleMethod.BICUBIC)
doc.resizeCanvas(UnitValue(CANVAS_WIDTH_HEIGHT,"px"),UnitValue(CANVAS_WIDTH_HEIGHT,"px"), AnchorPosition.MIDDLECENTER);

// Fill the doc to the screen
runMenuItem(app.charIDToTypeID("ActP"));